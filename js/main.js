/*

Теоретичні запитання:

  1.Які існують типи даних у Javascript?

            String;
            Number;
            Boolean;
            Null;
            Symbol;
            Undefined;
            Object;
            Bigint;
        
  2.У чому різниця між == і ===?

            У параметрах перевірки. Суворе додатково перевіряє відповідність типів даних без зміни / приведення.
            3 == "3" - true; 3 === "3" - false;
      
  3.Що таке оператор?

            Вбудована команда/функція в JS для выконання багатьох стандартних дій з операндами. 
            Приклад операторів - "+", "-", "*", "/", "=".
        
*/

let userName;
let userAge;


do {userName = prompt("Enter your Name", userName)

} while (isFinite(userName) | (userName === "null"));


do {userAge = prompt("Enter your Age", userAge ? null : '')

} while (isNaN(userAge) | +userAge === 0);


if (userAge < 18) {

  document.querySelector("span").textContent =
    "You are not allowed to visit this website!";

  document.querySelector("span").style.backgroundColor = "red";
  document.querySelector("span").style.color = "white";

} else if (userAge >= 18 && userAge <= 22) {

  let result = confirm("Are you sure you want to continue?");

  if (result) {

    document.querySelector("span").textContent = "Welcome, " + userName + "!";
    document.querySelector("span").style.backgroundColor = "green";
    document.querySelector("span").style.color = "white";

  } else {

    document.querySelector("span").textContent =
      "You are not allowed to visit this website";

    document.querySelector("span").style.backgroundColor = "grey";
  }

} else if (userAge > 22) {

  document.querySelector("span").textContent = "Welcome, " + userName + "!";
  document.querySelector("span").style.backgroundColor = "chartreuse";
  document.querySelector("span").style.color = "white";
  
}
